(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("f74e8d46790f3e07fbb4a2c5dafe2ade0d8f5abc9c203cd1c29c7d5110a85230" "bddf21b7face8adffc42c32a8223c3cc83b5c1bbd4ce49a5743ce528ca4da2b6" "05626f77b0c8c197c7e4a31d9783c4ec6e351d9624aa28bc15e7f6d6a6ebd926" default))
 '(delete-selection-mode nil)
 '(package-selected-packages
   '(yaml-mode markdown-mode cmake-mode lua-mode gruber-darker-theme multiple-cursors go-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(setq-default indent-tabs-mode t)
(setq-default tab-width 4)
(defvaralias 'c-basic-offset 'tab-width)
(setq inhibit-startup-screen t)
(load-theme 'gruber-darker)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(setq-default truncate-lines t)
(setq display-line-numbers-type 'relative)
(electric-pair-mode t)
(global-display-line-numbers-mode)
(ido-mode t)
(set-frame-font "JetBrains Mono NL 12" nil t)

; Lua configuration
(setq-default indent-tabs-mode t lua-indent-level 4 tab-width lua-indent-level)

;; XML configuration
(setq-default indent-tabs-mode t nxml-child-indent 4 nxml-attribute-indent 4 nxml-slash-auto-complete-flag t)

;; Melpa
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)
										; Keyboard config
(defun move-text-internal (arg)
   (cond
    ((and mark-active transient-mark-mode)
     (if (> (point) (mark))
            (exchange-point-and-mark))
     (let ((column (current-column))
              (text (delete-and-extract-region (point) (mark))))
       (forward-line arg)
       (move-to-column column t)
       (set-mark (point))
       (insert text)
       (exchange-point-and-mark)
       (setq deactivate-mark nil)))
    (t
     (beginning-of-line)
     (when (or (> arg 0) (not (bobp)))
       (forward-line)
       (when (or (< arg 0) (not (eobp)))
            (transpose-lines arg))
       (forward-line -1)))))
(defun move-text-down (arg)
   "Move region (transient-mark-mode active) or current line
  arg lines down."
   (interactive "*p")
   (move-text-internal arg))
(defun move-text-up (arg)
   "Move region (transient-mark-mode active) or current line
  arg lines up."
   (interactive "*p")
   (move-text-internal (- arg)))
(defun gcm-scroll-down ()
  (interactive)
  (scroll-up 4))
(defun gcm-scroll-up ()
  (interactive)
  (scroll-down 4))
(defalias 'exit 'save-buffers-kill-terminal)
(defun redefine-keys (_file)
  (global-set-key (kbd "<M-up>") 'move-text-up)
  (global-set-key (kbd "<M-down>") 'move-text-down)
  (global-set-key (kbd "M-n") 'gcm-scroll-down)
  (global-set-key (kbd "M-p") 'gcm-scroll-up)
  (global-set-key (kbd "<f5>") 'fzf-directory)
  )
(add-hook 'after-load-functions 'redefine-keys)
(setq backup-directory-alist `((".*".,"~/.emacs.d/temporary")))
(setq auto-save-file-name-transforms`((".*","~/.emacs.d/temporary" t)))
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
